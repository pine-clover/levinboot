/* SPDX-License-Identifier: CC0-1.0 */
#pragma once

#define PRIu64 "zu"
#define PRIx64 "zx"
#define PRIu32 "u"
#define PRIx32 "x"
#define PRIu16 "u"
#define PRIx16 "x"
#define PRIu8 "u"
#define PRIx8 "x"
